#!/bin/sh

vpnactive="$(nmcli -t -f uuid,type c show --active | grep "vpn" | rev | cut -c 5- | rev)"

vpnrandom="$(nmcli -t -f uuid,type c | grep "vpn" | rev | cut -c 5- | rev | sort -R | head -n 1)"

nmcli c down $vpnactive
nmcli c up $vpnrandom
