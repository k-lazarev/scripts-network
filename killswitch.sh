#!/bin/sh

# Should be owned by root
# Place to /etc/NetworkManager/dispatcher.d

killswitch()
{
    result=$(nmcli dev | grep "tun" | grep -w "connected")
    if [ -n "$result" ]; then
        true
    else
        killall chrome # Or anything else
    fi
}

if [ "$2" = "up" ]; then
    killswitch
fi

if [ "$2" = "down" ]; then
    killswitch
fi
